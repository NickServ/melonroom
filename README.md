# Melon Room
A simple clicker game made in Wiremod Expression 2.
## How to play
Spawn as many melons as you can by pressing Space or N, but not when the developer is around (no graphics, argh).
You can tell when the invisible developer is around based on "verbal" cues given by your accomplice at the top of the screen.
You need to spawn at least 400 melons to win, otherwise your accomplice will votekick you.
If you spawn melons while the developer is around, you will be banned for melon spam.
## License
Everything in this repository I made 100% by myself. I hereby release all of it into the public domain, and under the Creative Commons CC0 1.0 license.
A copy of the CC0 license is in the LICENSE file of this repository.
# Versions
## Melon Room
The original version, using a Console Screen output and Keyboard input.
## Melon Room Ultrahard Mode
This version features more challenging gameplay, taunting messages, and better payouts.
I made it because someone had made their own ultrahard mode before me and I wanted the spotlight back on me. Vain, I know.
It was based on an old version of the game and not kept up to date, so it still uses `elseif` messes instead of switch cases.
## Melon Room 1.5
This was an upgraded version of Melon Room using EGP instead of the Console Screen.
It is branded internally as "Melon Room 2 (Legacy)" and before that "Melon Room (EGP)".
Gameplay improvements and changes were planned, but these ideas never made it into this version as they were being saved for a proper sequel.
This was playable and looks a lot better, although the melons are much less detailed and the developer is *still* invisible.
## Melon Room 2
Unfortunately, the aforementioned "proper sequel" never made it very far. The file is nearly empty.
I wasn't really in the mood to be making games around this time, so it was doomed from the start and it shows in the code.
Maybe I'll pick it up again in another language.